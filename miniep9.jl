using LinearAlgebra
using Test

function multiplica(a, b)
	dima = size(a)
	dimb = size(b)
	if dima[2] != dimb[1]
		return -1
	end
	c = zeros(dima[1], dimb[2])
	for i in 1:dima[1]
		for j in 1:dimb[2]
			for k in 1:dima[2]
				c[i, j] = c[i, j] + a[i, k] * b[k, j]
			end
		end
	end
	return c
end

function matrix_pot(M, p)
	M_aux = M 
	if p == 1
		return M
	end
	while p > 1
		M = multiplica(M, M_aux)
		p -= 1
	end
	return M
end

function matrix_pot_by_squaring(M, p)
	M_squared = multiplica(M, M)
	if p == 1
		return M
	elseif p % 2 == 0
		return matrix_pot_by_squaring(M_squared, p/2)
	else
		return M * matrix_pot_by_squaring(M_squared, (p-1)/2)
	end
end	

function compare_times()
	M = Matrix(LinearAlgebra.I, 30, 30)

	@time matrix_pot(M, 10)
	@time matrix_pot_by_squaring(M, 10)
end

#compare_times()

@test matrix_pot([1 2 ; 3 4], 1) == [1 2 ; 3 4]
@test matrix_pot([1 2 ; 3 4], 2) == [7 10 ; 15 22]
@test matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7) == [564284320 470888932 323583236 351858636;
															     831242352 693529366 476618654 518192434;
																 577003992 481472568 330793288 359676984;
																 799037372 666708057 458121425 498126827]

@test matrix_pot([1 2 ; 3 4], 2) == matrix_pot_by_squaring([1 2 ; 3 4], 2)
@test matrix_pot([1 2 ; 3 4], 3) == matrix_pot_by_squaring([1 2 ; 3 4], 3)
@test matrix_pot([1 2 ; 3 4], 5) == matrix_pot_by_squaring([1 2 ; 3 4], 5)
@test matrix_pot([1 2 ; 3 4], 10) == matrix_pot_by_squaring([1 2 ; 3 4], 10)
@test matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7) == matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7)
@test matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 10) == matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 10)

println("Tudo Ok")